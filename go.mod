module gitea.com/chenli/reverse

go 1.13

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gobwas/glob v0.2.3
	github.com/kr/pretty v0.2.1 // indirect
	github.com/lib/pq v1.7.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.4.0
	github.com/ziutek/mymysql v1.5.4
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
	xorm.io/xorm v1.1.0
)
